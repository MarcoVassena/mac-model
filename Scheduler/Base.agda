-- This module defines the basic features of a scheduler.

import Lattice as L

module Scheduler.Base (𝓛 : L.Lattice) where

open L.Lattice 𝓛

open import Data.Product
open import Data.Nat
open import Relation.Binary.PropositionalEquality

-- A scheduler Event l denotes the possible behaviours of a thread at
-- level l.
data Event (l : Label) : Set where
  Stuck : Event l
  Step : Event l
  Done : Event l
  Fork : (h : Label) (n : ℕ) -> l ⊑ h -> Event l

open Event public

-- Scheduler messages are used to orchestrate the interaction between
-- scheduler and global configuration.  A message ⟪ l , n , e ⟫
-- denotes that thread (l,n) is scheduled and that thread behaves
-- according to event e.
data Message : Label -> Set where
   ⟪_,_,_⟫ : (l : Label) (n : ℕ) (e : Event l) -> Message l

-- The interface of a scheduler.
record Scheduler : Set₁ where
  field

    -- The scheduler specific state type
    State : Set

    -- The scheduler semantics relation: ω ⟶ ω' ↑ m denotes that
    -- scheduler state ω gets updated to ω' and generates message m.
    _⟶_↑_ : ∀ {l} -> State -> State -> Message l -> Set

    -- Determinancy: The scheduler semantics relation is
    -- deterministic.
    determinismˢ : ∀ {l n e} {ω₁ ω₂ ω₃ : State} -> ω₁ ⟶ ω₂ ↑ ⟪ l , n , e ⟫ -> ω₁ ⟶ ω₃ ↑ ⟪ l , n , e ⟫ -> ω₂ ≡ ω₃

    -- (l,n) ∈ˢ ω denotes that thread (l,n) is alive according to scheduler state ω.
    _∈ˢ_ : Label × ℕ → State -> Set

  -- Next ω (l , n) encodes the fact that thread (l , n) is the next thread scheduled by ω, using
  -- the scheduler small-step semantics
  Next : State → Label × ℕ → Set
  Next ω (l , n) = ∀ e → ∃ (λ ω' → ω ⟶ ω' ↑ ⟪ l , n , e ⟫)
