-- This module defines some basic properties over lists.  While the
-- standard libraries provides something equivalent, these definitions
-- are less general but easier to use.

{-# OPTIONS --rewriting #-}

module Lemmas where

open import Data.List hiding (reverse ; drop)

open import Relation.Nullary

-- My own simple reverse function (no fold)
reverse : ∀ {A : Set} -> List A -> List A
reverse [] = []
reverse (x ∷ xs) = reverse xs ++ [ x ]

-- List membership
data _∈_ {A : Set} (x : A) : List A -> Set where
 here : ∀ {xs} -> x ∈ (x ∷ xs)
 there : ∀ {x' xs} -> x ∈ xs -> x ∈ (x' ∷ xs)

-- Membership in a reversed list.  We use this so that we don't have
-- to change index when stepping.
_∈ᴿ_ : ∀ {A} -> A -> List A -> Set
x ∈ᴿ Γ = x ∈ (reverse Γ)

-- Subset relation
data _⊆_ {A : Set} : List A -> List A -> Set where
  base : [] ⊆ []
  cons : ∀ {α xs₁ xs₂} -> xs₁ ⊆ xs₂ -> (α ∷ xs₁) ⊆ (α ∷ xs₂)
  drop : ∀ {α xs₁ xs₂} -> xs₁ ⊆ xs₂ -> xs₁ ⊆ (α ∷ xs₂)

infixr 2 _⊆_

-- ⊆ is reflexive
refl-⊆ : ∀ {A} {xs : List A} -> xs ⊆ xs
refl-⊆ {_} {[]} = base
refl-⊆ {_} {x ∷ xs} = cons refl-⊆

--------------------------------------------------------------------------------

-- Helper lemmas
prod-⊆ : ∀ {A : Set} {x : A} {xs₁ xs₂} -> xs₁ ⊆ xs₂ -> xs₁ ⊆ xs₂ ++ [ x ]
prod-⊆ base = drop base
prod-⊆ (cons x) = cons (prod-⊆ x)
prod-⊆ (drop x) = drop (prod-⊆ x)

snoc-⊆ : ∀ {A : Set} {x : A} {xs₁ xs₂} -> xs₁ ⊆ xs₂ -> xs₁ ++ [ x ] ⊆ xs₂ ++ [ x ]
snoc-⊆ base = cons base
snoc-⊆ (cons x) = cons (snoc-⊆ x)
snoc-⊆ (drop x) = drop (snoc-⊆ x)

rev-⊆ : ∀ {A} {xs₁ xs₂ : List A} -> xs₁ ⊆ xs₂ -> reverse xs₁ ⊆ reverse xs₂
rev-⊆ base = base
rev-⊆ (cons x) = snoc-⊆ (rev-⊆ x)
rev-⊆ (drop x) = prod-⊆ (rev-⊆ x)

drop-⊆ : ∀ {A} {xs₁ xs₂ : List A} -> xs₁ ⊆ xs₁ ++ xs₂
drop-⊆ {_} {[]} {[]} = base
drop-⊆ {_} {[]} {x ∷ xs₂} = drop drop-⊆
drop-⊆ {_} {x ∷ xs₁} = cons drop-⊆

drop-⊆₂ : ∀ {A} {xs₁ xs₂ : List A} -> xs₁ ⊆ xs₂ ++ xs₁
drop-⊆₂ {xs₂ = []} = refl-⊆
drop-⊆₂ {xs₂ = x ∷ xs₂} = drop (drop-⊆₂ {xs₂ = xs₂})

++-⊆ : ∀ {A} {xs ys ys' : List A} → ys ⊆ ys' → xs ++ ys ⊆ xs ++ ys'
++-⊆ {xs = []} ys⊆ys' = ys⊆ys'
++-⊆ {xs = x ∷ xs} ys⊆ys' = cons (++-⊆ {xs = xs} ys⊆ys')

--------------------------------------------------------------------------------

-- If an element is present in a list, it is present in any superset
-- of the list.
wken-∈ : ∀ {A x} {xs₁ : List A} {xs₂ : List A} -> xs₁ ⊆ xs₂ -> x ∈ xs₁ -> x ∈ xs₂
wken-∈ base ()
wken-∈ (cons p) here = here
wken-∈ (cons p) (there q) = there (wken-∈ p q)
wken-∈ (drop p) q = there (wken-∈ p q)

-- Helper lemmas
snoc-∈ : ∀ {A} (x : A) (xs : List A) -> x ∈ (xs ++ [ x ])
snoc-∈ x [] = here
snoc-∈ x (_ ∷ xs) = there (snoc-∈ x xs)

∈-∈ᴿ : ∀ {A} {x : A}{xs} -> x ∈ xs -> x ∈ᴿ xs
∈-∈ᴿ {_} {x} {.x ∷ xs} here = snoc-∈ x (reverse xs)
∈-∈ᴿ {_} {_} {x' ∷ xs} (there x) = wken-∈ drop-⊆ (∈-∈ᴿ x)

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality hiding ([_])
open import Data.Empty

{-# BUILTIN REWRITE _≡_ #-}

rev-append-≡ : ∀ {A x} -> (xs : List A) -> reverse (xs ++ [ x ]) ≡ x ∷ reverse xs
rev-append-≡ [] = refl
rev-append-≡ {_} {x} (x₁ ∷ xs) rewrite rev-append-≡ {_} {x} xs = refl

{-# REWRITE  rev-append-≡ #-}

rev-rev-≡ : ∀ {A : Set} -> (xs : List A) -> reverse (reverse xs) ≡ xs
rev-rev-≡ [] = refl
rev-rev-≡ (x ∷ xs) = cong (_∷_ x) (rev-rev-≡ xs)

{-# REWRITE rev-rev-≡ #-}

∈ᴿ-∈ : ∀ {A} {x : A} {xs} -> x ∈ᴿ xs -> x ∈ xs
∈ᴿ-∈ {_} {xs} x = ∈-∈ᴿ x

wken-∈ᴿ : ∀ {A x} {xs₁ : List A} {xs₂ : List A} -> xs₁ ⊆ xs₂ -> x ∈ᴿ xs₁ -> x ∈ᴿ xs₂
wken-∈ᴿ x p = wken-∈ (rev-⊆ x) p

hereᴿ : ∀ {A : Set} {{xs}} {x : A} -> x ∈ᴿ (x ∷ xs)
hereᴿ {{xs}} {x} = snoc-∈ x (reverse xs)

++[] : {A : Set} (xs : List A) -> xs ++ [] ≡ xs
++[] [] = refl
++[] (x ∷ xs) rewrite ++[] xs = refl

{-# REWRITE ++[] #-}

swap-∈ : ∀ {A} {x : A} (xs ys : List A) → x ∈ (xs ++ ys) → x ∈ (ys ++ xs)
swap-∈ [] ys p = p
swap-∈ (x ∷ xs) ys here = wken-∈ (drop-⊆₂ {xs₂ = ys}) here
swap-∈ (x ∷ xs) ys (there p) = wken-∈ (++-⊆ {xs = ys} (drop {_} {x} (refl-⊆ {_} {xs}))) (swap-∈ xs ys p)

--------------------------------------------------------------------------------

-- Could not find this in the standard library.
contrapositive : ∀ {A B : Set} -> (A -> B) ->  ¬ B -> ¬ A
contrapositive a⇒b ¬b a = ¬b (a⇒b a)

-- In the following, we reason about equivalence of stores, which have
-- a functional representation (i.e., Σ : (l : Label) -> Memory l).
-- There are several different notions of equality for functions. Here
-- we take the standard notion of extensional equivalence (∀ x . f x ≡
-- g x ⟹ f ≡ g), i.e., we consider two functions equal if they give
-- the same output on any input.  Since functional extensioanlity
-- cannot be proved within Agda itself, we introduce it as an axiom
-- using a postulate. It is known that this axiom is consistent with
-- Agda's underlying logics, hence this postulate does not invalidate
-- our proofs.
postulate fun-ext : ∀ {ℓ₁ ℓ₂} -> Extensionality ℓ₁ ℓ₂
