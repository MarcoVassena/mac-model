-- This modules defines the types of MAC.  Note that the module is
-- parametrized by the lattice 𝓛, since types carry around labels.
import Lattice as L

module Types (𝓛 : L.Lattice) where

open L.Lattice 𝓛 public

open import Data.Fin using (Fin ; zero ; suc) public
open import Data.Unit hiding (_≤_ ; _≟_) public
open import Data.Product using (Σ ; _×_ ; _,_)
open import Data.Maybe using (Maybe ; just ; nothing)

-- Standard Types τ
data Ty : Set where
  （） : Ty
  Bool : Ty
  χ : Ty                           -- Exception type
  _➔_ : (τ₁ t₂ : Ty) -> Ty
  Mac : (l : Label) -> Ty -> Ty    -- Mac computation
  Labeled : Label -> Ty -> Ty      -- Labeled value
  Ref : (l : Label) -> Ty -> Ty    -- Labeled mutable reference
  MVar : (l : Label) -> Ty -> Ty   -- Labeled synchronization variable
  Addr : Ty                        -- Reference Address

infixr 3 _➔_

--------------------------------------------------------------------------------

open import Data.List hiding (drop ; reverse ; take) public
open import Lemmas public

-- Typing judmgent context. Our calculus De Bruijn indexes, i.e.,
-- variables are unamed and identified by their position in the
-- context.
Context : Set
Context = List Ty

--------------------------------------------------------------------------------
