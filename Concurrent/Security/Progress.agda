-- In this module, we prove progress, i.e., we show that if global
-- configuration steps with a low thread, then any low-equivalent
-- configuration also steps.

import Lattice as L₁
import Scheduler as S₁
import Scheduler.Security as Sch

module Concurrent.Security.Progress {𝓛 : L₁.Lattice} {𝓢 : S₁.Scheduler 𝓛} (A : L₁.Label 𝓛) (𝓝 : Sch.NIˢ 𝓛 A 𝓢) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛

open import Concurrent.Security.LowEq A 𝓝 as L
open import Concurrent.Calculus 𝓛 𝓢 as C
open import Concurrent.Semantics 𝓛 𝓢

open import Sequential.Valid 𝓛 using (stateᴾ ; validᴾ)
open import Concurrent.Valid 𝓛 𝓢 using (validᴳ ; readᴾ-valid)

open import Scheduler.Base 𝓛
open import Scheduler.Security 𝓛 A
module S =  NIˢ 𝓝

open import Function
open import Data.Nat
open import Data.Sum
open import Data.Product as P
open import Data.Empty
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

--------------------------------------------------------------------------------
-- Helper lemmas

-- A fork∙(t) sequential event denotes that a configuration is not
-- valid (only invalid construct fork∙ triggers that event).
fork∙-invalid : ∀ {l h τ t} {l⊑h : l ⊑ h} {p₁ p₂ : Program (Mac l τ)} -> (step : p₁ ⟼ p₂) -> event step ≡ (fork∙ l⊑h t) -> ¬ (validᴾ p₁)
fork∙-invalid (Lift x) () (vˢ , vᵗ)
fork∙-invalid (Label' l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Unlabel₁ l⊑h step) () (vˢ , vᵗ)
fork∙-invalid (Unlabel₂ l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Unlabelχ l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Bind₁ step) s≡f (vˢ , vᵗ) = fork∙-invalid step s≡f (vˢ , proj₁ vᵗ)
fork∙-invalid Bind₂ () (vˢ , vᵗ)
fork∙-invalid Bind₃ () (vˢ , vᵗ)
fork∙-invalid (Join l⊑h x) () (vˢ , vᵗ)
fork∙-invalid (Joinχ l⊑h x) () (vˢ , vᵗ)
fork∙-invalid (Join∙ l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Catch₁ step) s≡f (vˢ , vᵗ) = fork∙-invalid step s≡f (vˢ , proj₁ vᵗ)
fork∙-invalid Catch₂ () (vˢ , vᵗ)
fork∙-invalid Catch₃ () (vˢ , vᵗ)
fork∙-invalid (New l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Write₁ l⊑h step) () (vˢ , vᵗ)
fork∙-invalid (Write₂ l⊑h w) () (vˢ , vᵗ)
fork∙-invalid (Read₁ l⊑h step) () (vˢ , vᵗ)
fork∙-invalid (Read₂ l⊑h r) () (vˢ , vᵗ)
fork∙-invalid (New∙ l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Write∙₁ l⊑h step) () (vˢ , vᵗ)
fork∙-invalid (Write∙₂ l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Fork l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Fork∙ l⊑h) refl (vˢ , vᵗ) = ⊥-elim vᵗ
fork∙-invalid (NewMVar l⊑h) () (vˢ , vᵗ)
fork∙-invalid (Take₁ step) () (vˢ , vᵗ)
fork∙-invalid (Take₂ r w) () (vˢ , vᵗ)
fork∙-invalid (Put₁ step) () (vˢ , vᵗ)
fork∙-invalid (Put₂ r w) () (vˢ , vᵗ)
fork∙-invalid (NewMVar∙ l⊑h) () (vˢ , vᵗ)

-- If a program triggers a fork event, then the store remains
-- unchanged by the step.
fork-step-≡ : ∀ {L H τ} {t : Thread H} {p p' : Program (Mac L τ)} (L⊑H : L ⊑ H) (step : p ⟼ p') →
                   event step ≡ fork L⊑H t → store p ≡ store p'
fork-step-≡ L⊑A (Lift x) ()
fork-step-≡ L⊑A (Label' l⊑h) ()
fork-step-≡ L⊑A (Unlabel₁ l⊑h step) ()
fork-step-≡ L⊑A (Unlabel₂ l⊑h) ()
fork-step-≡ L⊑A (Unlabelχ l⊑h) ()
fork-step-≡ L⊑A (Bind₁ step) eq = fork-step-≡ L⊑A step eq
fork-step-≡ L⊑A Bind₂ ()
fork-step-≡ L⊑A Bind₃ ()
fork-step-≡ L⊑A (Join l⊑h x) ()
fork-step-≡ L⊑A (Joinχ l⊑h x) ()
fork-step-≡ L⊑A (Join∙ l⊑h) ()
fork-step-≡ L⊑A (Catch₁ step) eq = fork-step-≡ L⊑A step eq
fork-step-≡ L⊑A Catch₂ ()
fork-step-≡ L⊑A Catch₃ ()
fork-step-≡ L⊑A (New l⊑h) ()
fork-step-≡ L⊑A (Write₁ l⊑h step) ()
fork-step-≡ L⊑A (Write₂ l⊑h w) ()
fork-step-≡ L⊑A (Read₁ l⊑h step) ()
fork-step-≡ L⊑A (Read₂ l⊑h r) ()
fork-step-≡ L⊑A (New∙ l⊑h) ()
fork-step-≡ L⊑A (Write∙₁ l⊑h step) ()
fork-step-≡ L⊑A (Write∙₂ l⊑h) ()
fork-step-≡ L⊑A (Fork l⊑h) refl = refl
fork-step-≡ L⊑A (Fork∙ l⊑h) ()
fork-step-≡ L⊑A (NewMVar l⊑h) ()
fork-step-≡ L⊑A (Take₁ step) ()
fork-step-≡ L⊑A (Take₂ r w) ()
fork-step-≡ L⊑A (Put₁ step) ()
fork-step-≡ L⊑A (Put₂ r w) ()
fork-step-≡ L⊑A (NewMVar∙ l⊑h) ()

-- If a thread is present in a pool, then we can write to the same
-- location and update it.
updateᴾ-valid : ∀ {l n t} {P : Pool l} → n ↦ t ∈ᴾ P → (t' : _) → ∃ (λ P' → P' ≔ P [ n ↦ t' ]ᴾ)
updateᴾ-valid here t = _ , here
updateᴾ-valid (there r) t = P.map (_◅_ _) there (updateᴾ-valid r t)


-- If we know that a global configuration is valid and the scheduler
-- schedules an existing thread then, we can reconstruct the step of
-- that configuration, where that thread is run.  To do that, we
-- inspect the status of the program (the memory and the scheduled
-- thread) and choose the appropriate concurrent step.
mkStep : ∀ {l n t} {c : Global} (v : validᴳ c) ->
              n ↦ t ∈ᴾ (mapᴾ c l) -> Stateᴾ ⟨ store c , t ⟩ ->
              S.Next (state c) (l , n) -> Redexᴳ (l , n) c
mkStep v r (inj₁ (Step step)) next with event step | inspect event step
mkStep v r (inj₁ (Step step)) next | ∅ | [ e≡∅ ] = Step (Step (proj₂ (next Step)) r step e≡∅ (proj₂ (updateᴾ-valid r _)))
mkStep {L} {c = c} v r (inj₁ (Step step)) next | fork {h = H} L⊑H t | [ e≡f ] with fork-step-≡ L⊑H step e≡f | updateᴾ-valid r _
... | refl | P' , u = Step (CFork L⊑H (proj₂ (next (Fork H (lengthᴾ (((mapᴾ c) [ L ↦ P' ]ᴹ) H)) L⊑H))) r step e≡f u)
mkStep (_ , vˢ , vᴹ) r (inj₁ (Step step)) next | fork∙ l⊑h t | [ e≡f∙ ] = ⊥-elim (fork∙-invalid step e≡f∙ (vˢ , readᴾ-valid r (vᴹ _)))
mkStep v r (inj₂ (inj₁ done)) next = Step (Done (proj₂ (next Done)) r done)
mkStep v r (inj₂ (inj₂ stuck)) next = Step (Stuck (proj₂ (next Stuck)) r stuck)

-- There are two variants of 1-step progress progressᴸ and progressᴴ.
-- The main difference lies in the fact that when the first
-- configuration c₁ is aligned with the second, i.e., c₁ ≈⟨ i , 0 ⟩
-- c₂, then we know that the exact same thread is scheduled
-- (progressᴸ).  When they are not aligned, i.e., c₁ ≈⟨ i , 1 + j ⟩
-- c₂, then there is "some" other thread scheduled before (progressᴴ).

-- 1-step progress (low-step)
progressᴸ : ∀ {L n i} {c₁ c₂ c₁' : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}}
           -> L ⊑ A -> c₁ ≈ᴳ⟨ i , 0 ⟩ c₂ -> ( L , n ) ⊢ c₁ ↪ c₁' -> Redexᴳ ( L , n ) c₂
progressᴸ {L} {n} {c₁ = C.⟨ ω₁ , Σ₁ , Φ₁ ⟩} {c₂ = C.⟨ ω₂ , Σ₂ , Φ₂ ⟩} {{v₁ = v₁}} {{vˢ , Σⱽ , vᴹ}} L⊑A L.⟨ ω₁≈ω₂ , Σ₁≈Σ₂ , Φ₁≈Φ₂ ⟩ step
  with S.progressᴸ L⊑A (getSchStep step) ω₁≈ω₂
... | Ln∈ω₁ , nextˢ with vˢ (L , n) Ln∈ω₁
... | t , r = mkStep (vˢ , Σⱽ , vᴹ) r (stateᴾ (⟨ Σ₂ , t ⟩) {{ Σⱽ , (readᴾ-valid r (vᴹ L)) }}) nextˢ

-- 1-step progress (high-step)
progressᴴ : ∀ {L i j n} {g₁ g₂ g₁' : Global} {{v₁ : validᴳ g₁}} {{v₂ : validᴳ g₂}} ->
                  L ⊑ A -> g₁ ≈ᴳ⟨ i , suc j ⟩ g₂ -> ( L , n ) ⊢ g₁ ↪ g₁' -> ∃ (λ x → Redexᴳ x g₂)
progressᴴ {g₁ = C.⟨ ω₁ , Σ₁ , Φ₁ ⟩} {g₂ = g₂} {{v₂ = vˢ , Σⱽ , vᴹ }} L⊑A L.⟨ ω₁≈ω₂ , Σ₁≈Σ₂ , Φ₁≈Φ₂ ⟩ step
  with S.progressᴴ L⊑A ω₁≈ω₂ (getSchStep step)
... | (H , m) , Ln∈ω₁ , nextˢ with vˢ (H , m) Ln∈ω₁
... | t , r = (H , m) , mkStep (vˢ , Σⱽ , vᴹ) r (stateᴾ (⟨ store g₂ , t ⟩) {{ Σⱽ , (readᴾ-valid r (vᴹ H)) }}) nextˢ
