-- This module defines low-equivalence for the concurrent calculus.
-- Just like module Sequential.Security.LowEq, it provides both the
-- intuitive definition as the kernel of the erasure function (≅) and
-- a "structural" version (≈), that simplifies certain proofs, and two
-- functions to go from one to the other (⌞_⌟ and ⌜_⌝).

import Lattice as L
import Scheduler as Sch
import Scheduler.Security as Sec

module Concurrent.Security.LowEq {𝓛 : L.Lattice} {𝓢 : Sch.Scheduler 𝓛} (A : L.Label 𝓛) (𝓝 : Sec.NIˢ 𝓛 A 𝓢) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛
open import Concurrent.Calculus 𝓛 𝓢
open import Concurrent.Security.Erasure A 𝓝
open import Concurrent.Security.Graph A 𝓝
open import Scheduler.Security 𝓛 A hiding (εᴹ)

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Data.Empty
open import Data.Product using (_×_)

--------------------------------------------------------------------------------

_≅ᴾ_ : ∀ {l} -> Pool l ->  Pool l -> Set
P₁ ≅ᴾ P₂ = εᴾ P₁ ≡ εᴾ P₂

-- Structural low-equivalence for Thread pool
data _≈ᴾ_ {l : Label} (P₁ : Pool l) (P₂ : Pool l) : Set where
  Kᴾ : ∀ {Pᴱ : Pool l} -> Eraseᴾ P₁ Pᴱ -> Eraseᴾ P₂ Pᴱ -> P₁ ≈ᴾ P₂

⌞_⌟ᴾ : ∀ {l} {P₁ P₂ : Pool l} {x : Dec (l ⊑ A)}-> P₁ ≈ᴾ P₂ -> P₁ ≅ᴾ P₂
⌞ Kᴾ e₁ e₂ ⌟ᴾ rewrite unlift-εᴾ e₁ | unlift-εᴾ e₂ = refl

⌜_⌝ᴾ : ∀ {l} {P₁ P₂ : Pool l} -> P₁ ≅ᴾ P₂ -> P₁ ≈ᴾ P₂
⌜_⌝ᴾ {_} {P₁} {P₂} eq with lift-εᴾ P₁ | lift-εᴾ P₂
... | e₁ | e₂ rewrite eq = Kᴾ e₁ e₂

import Sequential.Security.LowEq 𝓛 A as S

--------------------------------------------------------------------------------

-- Strucutral low-equivalence for Pools (point-wise)
data _≈ᴹ_ (Φ₁ Φ₂ : Mapᴾ) : Set where
  Kᴹ : ∀ {Φᴱ : Mapᴾ} -> Eraseᴹ Φ₁ Φᴱ -> Eraseᴹ Φ₂ Φᴱ -> Φ₁ ≈ᴹ Φ₂

_≅ᴹ_ : (Φ₁ Φ₂ : Mapᴾ) -> Set
Φ₁ ≅ᴹ Φ₂ =  εᴹ Φ₁ ≡ εᴹ Φ₂

⌞_⌟ᴹ : ∀ {Φ₁ Φ₂ : Mapᴾ} -> Φ₁ ≈ᴹ Φ₂ -> Φ₁ ≅ᴹ Φ₂
⌞ Kᴹ e₁ e₂ ⌟ᴹ rewrite unlift-εᴹ e₁ | unlift-εᴹ e₂ = refl

⌜_⌝ᴹ : ∀ {Φ₁ Φ₂ : Mapᴾ} -> Φ₁ ≅ᴹ Φ₂ -> Φ₁ ≈ᴹ Φ₂
⌜_⌝ᴹ {Φ₁} {Φ₂} eq = Kᴹ (lift-εᴹ Φ₁) (flip (sym eq) (lift-εᴹ Φ₂))
  where flip : ∀ {Φ₂' Φ₂''} -> Φ₂' ≡ Φ₂'' -> Eraseᴹ Φ₂ Φ₂' -> Eraseᴹ Φ₂ Φ₂''
        flip refl e = e

refl-≈ᴹ : ∀ {Φ : Mapᴾ} ->  Φ ≈ᴹ Φ
refl-≈ᴹ = ⌜ refl ⌝ᴹ

sym-≈ᴹ :  ∀ {Φ₁ Φ₂ : Mapᴾ} -> Φ₁ ≈ᴹ Φ₂ -> Φ₂ ≈ᴹ Φ₁
sym-≈ᴹ x  = ⌜ sym ⌞ x ⌟ᴹ ⌝ᴹ

trans-≈ᴹ :  ∀ {Φ₁ Φ₂ Φ₃ : Mapᴾ} -> Φ₁ ≈ᴹ Φ₂ -> Φ₂ ≈ᴹ Φ₃ -> Φ₁ ≈ᴹ Φ₃
trans-≈ᴹ x y = ⌜ trans ⌞ x ⌟ᴹ ⌞ y ⌟ᴹ ⌝ᴹ

--------------------------------------------------------------------------------

_≅ᴳ_ : (c₁ c₂ : Global) -> Set
c₁ ≅ᴳ c₂ = εᴳ c₁ ≡ εᴳ c₂

-- structural low-equivalence for global configuration
record _≈ᴳ_ (c₁ c₂ : Global) : Set where
  constructor ⟨_,_,_⟩
  open NIˢ 𝓝 public
  field
    state-≈ : state c₁ ≈ˢ state c₂
    store-≈ : store c₁ S.≈ˢ store c₂
    map-≈ : mapᴾ c₁ ≈ᴹ mapᴾ c₂

⌜_⌝ᴳ : ∀ {c₁ c₂ : Global} -> c₁ ≅ᴳ c₂ -> c₁ ≈ᴳ c₂
⌜_⌝ᴳ {⟨ ω₁ , Σ₁ , Φ₁ ⟩} {⟨ ω₂ , Σ₂ , Φ₂ ⟩} eq = ⟨ ⌜ aux₁ eq ⌝ˢ , S.⌜ aux₂ eq ⌝ˢ , ⌜ aux₃ eq ⌝ᴹ ⟩
  where open NIˢ 𝓝

        aux₁ : ∀ {ω₁ ω₂ Σ₁ Σ₂ Φ₁ Φ₂} -> _≡_ {_} {Global} ⟨ ω₁ , Σ₁ , Φ₁ ⟩ ⟨ ω₂ , Σ₂ , Φ₂ ⟩ -> ω₁ ≡ ω₂
        aux₁ refl = refl

        aux₂ : ∀ {ω₁ ω₂ Σ₁ Σ₂ Φ₁ Φ₂} -> _≡_ {_} {Global} ⟨ ω₁ , Σ₁ , Φ₁ ⟩ ⟨ ω₂ , Σ₂ , Φ₂ ⟩ -> Σ₁ ≡ Σ₂
        aux₂ refl = refl

        aux₃ : ∀ {ω₁ ω₂ Σ₁ Σ₂ Φ₁ Φ₂} -> _≡_ {_} {Global} ⟨ ω₁ , Σ₁ , Φ₁ ⟩ ⟨ ω₂ , Σ₂ , Φ₂ ⟩ -> Φ₁ ≡ Φ₂
        aux₃ refl = refl

⌞_⌟ᴳ : ∀ {c₁ c₂ : Global} -> c₁ ≈ᴳ c₂ -> c₁ ≅ᴳ c₂
⌞ ⟨ ω₁≈ω₂ , Σ₁≈Σ₂ , Φ₁≈Φ₂ ⟩ ⌟ᴳ = lift-εᴳ ⌞ ω₁≈ω₂ ⌟ˢ S.⌞ Σ₁≈Σ₂ ⌟ˢ ⌞ Φ₁≈Φ₂ ⌟ᴹ
  where open NIˢ 𝓝
        lift-εᴳ : ∀ {ω₁ ω₂ Σ₁ Σ₂ Φ₁ Φ₂} -> ω₁ ≡ ω₂ ->  Σ₁ ≡ Σ₂ -> Φ₁ ≡ Φ₂ ->
           _≡_ {_} {Global} ⟨ ω₁ , Σ₁ , Φ₁ ⟩ ⟨ ω₂ , Σ₂ , Φ₂ ⟩
        lift-εᴳ eq₁ eq₂ eq₃ rewrite eq₁ | eq₂ | eq₃ = refl

refl-≈ᴳ : ∀ {g : Global} -> g ≈ᴳ g
refl-≈ᴳ = ⌜ refl  ⌝ᴳ

sym-≈ᴳ : ∀ {c₁ c₂ : Global} -> c₁ ≈ᴳ c₂ -> c₂ ≈ᴳ c₁
sym-≈ᴳ x = ⌜ sym ⌞ x ⌟ᴳ ⌝ᴳ

trans-≈ᴳ : ∀ {c₁ c₂ g₃ : Global} -> c₁ ≈ᴳ c₂ -> c₂ ≈ᴳ g₃ -> c₁ ≈ᴳ g₃
trans-≈ᴳ x y = ⌜ trans ⌞ x ⌟ᴳ ⌞ y ⌟ᴳ ⌝ᴳ

--------------------------------------------------------------------------------

open import Data.Nat

-- Lifts annotations in the scheduler to configurations
record _≈ᴳ⟨_,_⟩_ (c₁ : Global) (n₁ : ℕ) (n₂ : ℕ) (c₂ : Global) : Set where
  constructor ⟨_,_,_⟩
  open NIˢ 𝓝 public
  field
    state-≈' : (state c₁) ≈ˢ⟨ n₁ , n₂ ⟩ (state c₂)
    store-≈' : store c₁ S.≈ˢ store c₂
    map-≈' : mapᴾ c₁ ≈ᴹ mapᴾ c₂

open NIˢ 𝓝

alignᴳ : ∀ {c₁ c₂ : Global} -> (c₁≈c₂ : c₁ ≈ᴳ c₂) ->
         let ω₁≈ω₂ = _≈ᴳ_.state-≈ c₁≈c₂ in
           c₁ ≈ᴳ⟨ offset₁ ω₁≈ω₂ , offset₂ ω₁≈ω₂ ⟩ c₂
alignᴳ ⟨ state-≈ , store-≈ , map-≈ ⟩ = ⟨ align state-≈ , store-≈ , map-≈ ⟩

forgetᴳ : ∀ {n₁ n₂} {c₁ c₂ : Global} -> c₁ ≈ᴳ⟨ n₁ , n₂ ⟩ c₂ -> c₁ ≈ᴳ c₂
forgetᴳ ⟨ state-≈' , store-≈' , map-≈' ⟩ = ⟨ forget state-≈' , store-≈' , map-≈' ⟩
