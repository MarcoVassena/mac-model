-- This module defines the basics features of the concurrent calculus.

import Lattice as L
import Scheduler as S

module Concurrent.Calculus (𝓛 : L.Lattice) (𝓢 : S.Scheduler 𝓛) where

open import Data.Nat hiding (_≟_)
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

open import Types 𝓛
open import Sequential.Calculus 𝓛
open S.Scheduler 𝓛 𝓢

--------------------------------------------------------------------------------

-- Thread l is a short-hand for a term that represents a thread at
-- security level l.
Thread : Label -> Set
Thread l = CTerm (Mac l （）)

-- Pool of threads at a certain security level
data Pool (l : Label) : Set where
  [] : Pool l
  _◅_ : (t : Thread l) (T : Pool l) -> Pool l

  -- An erased pool
  ∙ : Pool l

infixr 3 _◅_

-- Computes the length of a thread pool
lengthᴾ : ∀ {l} -> Pool l -> ℕ
lengthᴾ [] = 0
lengthᴾ (x ◅ P) = suc (lengthᴾ P)
lengthᴾ ∙ = 0

-- Enqueue a thread in a thread pool
_▻_ : ∀ {l} -> Pool l -> Thread l -> Pool l
[] ▻ t = t ◅ []
(x ◅ ts) ▻ t = x ◅ (ts ▻ t)
∙ ▻ t = ∙

-- Mapᴾ is the pool map, which maps each label to the corrisponding labeled pool
Mapᴾ : Set
Mapᴾ = (l : Label) → Pool l

-- The global configuration contains the scheduler state, the store and the thread pool map.
record Global : Set where
  constructor ⟨_,_,_⟩
  field state : State
        store : Store
        mapᴾ : Mapᴾ

open Global public

--------------------------------------------------------------------------------
-- Thread pool operations (read and write) reified as dependent types.
-- Since these operations are partial, it is customary in Agda to
-- exploit dependent types to encode only the well-defined behaviour,
-- i.e., reading and writing to valid addresses.

-- Lookupᴾ t n P is the proof that the n-th thread in thread pool P
-- contains thread t : P[ n ] = t
data Lookupᴾ {l : Label} (t : Thread l) : ℕ -> Pool l -> Set where
  here : ∀ {ts : Pool l} -> Lookupᴾ t zero (t ◅ ts)
  there : ∀ {n} {ts : Pool l} {t' : Thread l} -> Lookupᴾ t n ts -> Lookupᴾ t (suc n) (t' ◅ ts)

-- Sytactic sugar for Lookupᴾ
_↦_∈ᴾ_ : ∀ {l} -> ℕ -> Thread l -> Pool l -> Set
n ↦ t ∈ᴾ ts = Lookupᴾ t n ts

-- Writeᴹ t n P₁ P₂ is the proof that updating thread pool P₁ with t
-- at position n gives thread pool P₂: P₂ ≔ P₁ [ n ↦ t ]
data Writeᴾ {l : Label} (t : Thread l) : ℕ -> Pool l -> Pool l -> Set where
  here : ∀ {ts : Pool l} {t' : Thread l} -> Writeᴾ t zero (t' ◅ ts) (t ◅ ts)
  there : ∀ {n} {ts₁ ts₂ : Pool l} {t' : Thread l} -> Writeᴾ t n ts₁ ts₂ -> Writeᴾ t (suc n) (t' ◅ ts₁) (t' ◅ ts₂)

-- Syntatic sugar for overwriting a pool.
_≔_[_↦_]ᴾ : ∀ {l} -> Pool l -> Pool l -> ℕ -> Thread l -> Set
P' ≔ P [ n ↦ t ]ᴾ = Writeᴾ t n P P'

--------------------------------------------------------------------------------
-- Thread pool map operations

-- Updates a thread pool map.
_[_↦_]ᴹ : Mapᴾ -> (l : Label) -> Pool l -> Mapᴾ
_[_↦_]ᴹ Φ l P l' with l ≟ l'
... | yes refl = P
... | no ¬p = Φ l'

infixr 3  _[_↦_]ᴹ

--------------------------------------------------------------------------------

open import Function

-- If you can write to it, then you can read it
write-∈ᴾ : ∀ {l n} {P P' : Pool l} {t : Thread l} → P' ≔ P [ n ↦ t ]ᴾ → n ↦ t ∈ᴾ P'
write-∈ᴾ here = here
write-∈ᴾ (there w) = there (write-∈ᴾ w)
