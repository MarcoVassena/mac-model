-- This module re-exports the main security results needed in the rest
-- of the model. These modules are paremtrized by the lattice (𝓛) and
-- the attacker level A.

import Lattice as L

module Sequential.Security (𝓛 : L.Lattice) (A : L.Label 𝓛) where

open import Sequential.Security.LowEq 𝓛 A public
open import Sequential.Security.PINI 𝓛 A public
