-- This module proves progress insensitive non-interference
import Lattice as L

module Sequential.Security.PINI (𝓛 : L.Lattice) (A : L.Label 𝓛) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛
open import Sequential.Determinism 𝓛
open import Sequential.Security.Erasure 𝓛 A
open import Sequential.Security.LowEq 𝓛 A
open import Sequential.Security.Simulation 𝓛 A

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Data.Empty


-- Low-equivalence preservation
≅ᴾ-preservation  : ∀ {l τ} {p₁ p₁' p₂ p₂' : Program (Mac l τ)} -> p₁ ≅ᴾ p₂ -> p₁ ⟼ p₁' -> p₂ ⟼ p₂' -> p₁' ≅ᴾ p₂'
≅ᴾ-preservation {l} eq s₁ s₂ = aux eq (εᴾ-sim (l ⊑? A) s₁) (εᴾ-sim (l ⊑? A) s₂)
  where aux : ∀ {l τ} {p₁ p₁' p₂ p₂' : Program (Mac l τ)} -> p₁ ≡ p₂ -> p₁ ⟼ p₁' -> p₂ ⟼ p₂' -> p₁' ≡ p₂'
        aux refl x y = determinismᴾ x y

-- Low-equivalence preservation. This variant requires an explicit
-- proof object about the visibility of the security level of the
-- attacker.
≅ᴾ-preservation' : ∀ {l τ} {p₁ p₁' p₂ p₂' : Program (Mac l τ)} (x : Dec (l ⊑ A)) -> p₁ ≅ᴾ⟨ x ⟩ p₂ -> p₁ ⟼ p₁' -> p₂ ⟼ p₂' -> p₁' ≅ᴾ⟨ x ⟩ p₂'
≅ᴾ-preservation' x eq step₁ step₂ = ext-≅ᴾ {x = _ ⊑? A} {y = x} (≅ᴾ-preservation (ext-≅ᴾ {x = x} {y = _ ⊑? A} eq) step₁ step₂)

-- In a high-step the initial and final program are low-equivalent
εᴾ-simᴴ : ∀ {H τ} {H⋤A : H ⋤ A} → {p p' : Program (Mac H τ)} → p ⟼ p' → p ≅ᴾ⟨ no H⋤A ⟩ p'
εᴾ-simᴴ {H⋤A = H⋤A} {p = ⟨ Σ₁ , t₁ ⟩} {⟨ Σ₂ , t₂ ⟩} s rewrite stepᴴ-≡ H⋤A s = refl

-- Progress-Insensitive Non-Interference (PINI) for the sequential
-- calculus, where program termination is ignored.
pini : ∀ {l τ} {p₁ p₁' p₂ p₂' : Program (Mac l τ)} -> p₁ ≅ᴾ p₂ -> p₁ ⇓ p₁' -> p₂ ⇓ p₂' -> p₁' ≅ᴾ p₂'
pini p₁≅p₂ ⌞ ss₁ , v₁ ⌟ ⌞ ss₂ , v₂ ⌟ = aux (_ ⊑? A) p₁≅p₂ ss₁ v₁ ss₂ v₂
  where

    open import Sequential.Security.Progress 𝓛 A

    -- We use an auxiliary proof that explicitly unpacks the big-step
    -- to help the termination checker.  The interesting cases in this
    -- proof is the case where the two reductions have a different
    -- number of steps. If they are low programs, those cases are
    -- void: low programs will terminate in exactly the same number of
    -- steps. If the programs are high, this case is legitimate and
    -- proved by induction (high-steps preserve low-equivalence, see
    -- εᴾ-simᴴ).
    aux : ∀ {l τ} {p₁ p₁' p₂ p₂' : Program (Mac l τ)} (x : Dec (l ⊑ A)) ->
            p₁ ≅ᴾ⟨ x ⟩ p₂ -> p₁ ⟼⋆ p₁' → Doneᴾ p₁' →
                             p₂ ⟼⋆ p₂' → Doneᴾ p₂' → p₁' ≅ᴾ⟨ x ⟩ p₂'
    aux x p₁≅p₂ [] v₁ [] v₂ = p₁≅p₂
    aux (yes p) p₁≅p₂ [] v₁ (s ∷ ss₂) v₂ with val-ε v₁ | εᴾ-sim (yes p) s
    ... | v₁ᴱ | sᴱ rewrite sym p₁≅p₂ = ⊥-elim (¬doneSteps v₁ᴱ (Step sᴱ))
    aux (no H⋤A) p₁≅p₂ [] v₁ (s ∷ ss₂) v₂ = aux (no H⋤A) (trans p₁≅p₂ (εᴾ-simᴴ {H⋤A = H⋤A} s)) [] v₁ ss₂ v₂
    aux (yes p) p₁≅p₂ (s ∷ ss) v₁ [] v₂ with val-ε v₂ | εᴾ-sim (yes p) s
    ... | v₁ᴱ | sᴱ rewrite p₁≅p₂ = ⊥-elim (¬doneSteps v₁ᴱ (Step sᴱ))
    aux (no H⋤A) p₁≅p₂ (s ∷ ss₁) v₁ [] v₂ = aux (no H⋤A) (trans (sym (εᴾ-simᴴ {H⋤A = H⋤A} s)) p₁≅p₂) ss₁ v₁ [] v₂
    aux x p₁≅p₂ (s₁ ∷ ss₁) v₁ (s₂ ∷ ss₂) v₂ = aux x (≅ᴾ-preservation' x p₁≅p₂ s₁ s₂) ss₁ v₁ ss₂ v₂

--------------------------------------------------------------------------------
