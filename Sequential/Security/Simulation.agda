-- This module proves the single-step simulation property for the
-- sequential calculus.

import Lattice as L

-- A is the security level of the attacker
module Sequential.Security.Simulation (𝓛 : L.Lattice) (A : L.Lattice.Label 𝓛) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛
open import Sequential.Security.Erasure 𝓛 A

open import Data.Sum
open import Relation.Binary.PropositionalEquality hiding (subst ; [_])
open import Relation.Nullary
open import Data.Empty

-- Single step simulation for pure semantics
ε-sim : ∀ {τ : Ty} {t₁ t₂ : CTerm τ} -> t₁ ⇝ t₂ -> (ε t₁) ⇝ (ε t₂)
ε-sim (App x) = App (ε-sim x)
ε-sim Beta = Beta
ε-sim (If₁ x) = If₁ (ε-sim x)
ε-sim If₂ = If₂
ε-sim If₃ = If₃
ε-sim {Labeled l _} Fmap with l ⊑? A
ε-sim {Labeled l _} Fmap | yes p with l ⊑? A
ε-sim {Labeled l _} Fmap | yes _ | yes _ = Fmap
ε-sim {Labeled l _} Fmap | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim {Labeled l _} Fmap | no _ with l ⊑? A
ε-sim {Labeled l _} Fmap | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim {Labeled l _} Fmap | no _ | no _ = Fmap∙
ε-sim {Labeled l _} (⟨✴⟩₁ x) with l ⊑? A
... | yes _ = ⟨✴⟩₁ (ε-sim x)
... | no _ = ⟨✴⟩∙₁ (ε-sim x)
ε-sim {Labeled l _} (⟨✴⟩₂ x) with l ⊑? A
ε-sim {Labeled l x} (⟨✴⟩₂ x₁) | yes p with l ⊑? A
ε-sim {Labeled l x} (⟨✴⟩₂ x₁) | yes p₁ | yes p = ⟨✴⟩₂ (ε-sim x₁)
ε-sim {Labeled l x} (⟨✴⟩₂ x₁) | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim {Labeled l x} (⟨✴⟩₂ x₁) | no ¬p with l ⊑? A
ε-sim {Labeled l x} (⟨✴⟩₂ x₁) | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim {Labeled l x} (⟨✴⟩₂ x₁) | no ¬p₁ | no ¬p = ⟨✴⟩∙₂ (ε-sim x₁)
ε-sim {Labeled l x} ⟨✴⟩₃ with l ⊑? A
ε-sim {Labeled l x} ⟨✴⟩₃ | yes p with l ⊑? A
ε-sim {Labeled l x} ⟨✴⟩₃ | yes p₁ | yes p = ⟨✴⟩₃
ε-sim {Labeled l x} ⟨✴⟩₃ | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim {Labeled l x} ⟨✴⟩₃ | no ¬p with l ⊑? A
ε-sim {Labeled l x} ⟨✴⟩₃ | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim {Labeled l x} ⟨✴⟩₃ | no ¬p₁ | no ¬p = ⟨✴⟩∙₃
ε-sim {Labeled l τ} (Relabel₁ l⊑h x) with l ⊑? A
ε-sim {Labeled l τ} (Relabel₁ l⊑h x) | yes p = Relabel₁ l⊑h (ε-sim x)
ε-sim {Labeled l τ} (Relabel₁ l⊑h x) | no ¬p = Relabel∙₁ l⊑h (ε-sim x)
ε-sim {Labeled h τ} (Relabel₂ l⊑h) with h ⊑? A
ε-sim {Labeled h τ} (Relabel₂ {l = l} l⊑h) | yes p with l ⊑? A
ε-sim {Labeled h τ} (Relabel₂ l⊑h) | yes p₁ | yes p = Relabel₂ l⊑h
ε-sim {Labeled h τ} (Relabel₂ l⊑h) | yes h⊑A | no ¬p = ⊥-elim (¬p (trans-⊑ l⊑h h⊑A))
ε-sim {Labeled h τ} (Relabel₂ {l = l} l⊑h) | no ¬p with l ⊑? A
ε-sim {Labeled h τ} (Relabel₂ l⊑h) | no ¬p | yes p = Relabel∙₂ l⊑h
ε-sim {Labeled h τ} (Relabel₂ l⊑h) | no ¬p₁ | no ¬p = Relabel∙₂ l⊑h
ε-sim {Labeled h τ} (Relabel∙₁ l⊑h x) = Relabel∙₁ l⊑h (ε-sim x)
ε-sim (Relabel∙₂ {l = l} l⊑h) with l ⊑? A
ε-sim (Relabel∙₂ {h = h} l⊑h) | yes p with h ⊑? A
ε-sim (Relabel∙₂ l⊑h) | yes p₁ | yes p = Relabel∙₂ l⊑h
ε-sim (Relabel∙₂ l⊑h) | yes p | no ¬p = Relabel∙₂ l⊑h
ε-sim (Relabel∙₂ {h = h} l⊑h) | no ¬p with h ⊑? A
ε-sim (Relabel∙₂ l⊑h) | no ¬p | yes p = Relabel∙₂ l⊑h
ε-sim (Relabel∙₂ l⊑h) | no ¬p₁ | no ¬p = Relabel∙₂ l⊑h
ε-sim {Labeled l τ} Fmap∙ with l ⊑? A
ε-sim {Labeled l τ} Fmap∙ | yes p = Fmap∙
ε-sim {Labeled l τ} Fmap∙ | no ¬p = Fmap∙
ε-sim (⟨✴⟩∙₁ x) = ⟨✴⟩∙₁ (ε-sim x)
ε-sim (⟨✴⟩∙₂ {l = l} x) with l ⊑? A
ε-sim (⟨✴⟩∙₂ x) | yes p = ⟨✴⟩∙₂ (ε-sim x)
ε-sim (⟨✴⟩∙₂ x) | no ¬p = ⟨✴⟩∙₂ (ε-sim x)
ε-sim (⟨✴⟩∙₃ {l = l}) with l ⊑? A
ε-sim ⟨✴⟩∙₃ | yes p = ⟨✴⟩∙₃
ε-sim ⟨✴⟩∙₃ | no ¬p = ⟨✴⟩∙₃
ε-sim (⟨✴⟩₁χ {l = l} x) with l ⊑? A
ε-sim (⟨✴⟩₁χ {l = l} x) | yes p with l ⊑? A
ε-sim (⟨✴⟩₁χ x) | yes p₁ | yes p = ⟨✴⟩₁χ (ε-sim x)
ε-sim (⟨✴⟩₁χ x) | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim (⟨✴⟩₁χ {l = l} x) | no ¬p with l ⊑? A
ε-sim (⟨✴⟩₁χ x) | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim (⟨✴⟩₁χ x) | no ¬p₁ | no ¬p = ⟨✴⟩∙₂ (ε-sim x)
ε-sim (⟨✴⟩₂χ {l = l}) with l ⊑? A
ε-sim (⟨✴⟩₂χ {l = l}) | yes p with l ⊑? A
ε-sim ⟨✴⟩₂χ | yes p₁ | yes p = ⟨✴⟩₂χ
ε-sim ⟨✴⟩₂χ | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim (⟨✴⟩₂χ {l = l}) | no ¬p with l ⊑? A
ε-sim ⟨✴⟩₂χ | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim ⟨✴⟩₂χ | no ¬p₁ | no ¬p = ⟨✴⟩∙₃
ε-sim (⟨✴⟩₃χ {l = l}) with l ⊑? A
ε-sim (⟨✴⟩₃χ {l = l}) | yes p with l ⊑? A
ε-sim ⟨✴⟩₃χ | yes p₁ | yes p = ⟨✴⟩₃χ
ε-sim ⟨✴⟩₃χ | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim (⟨✴⟩₃χ {l = l}) | no ¬p with l ⊑? A
ε-sim ⟨✴⟩₃χ | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim ⟨✴⟩₃χ | no ¬p₁ | no ¬p = ⟨✴⟩∙₃
ε-sim (⟨✴⟩₄χ {l = l}) with l ⊑? A
ε-sim (⟨✴⟩₄χ {l = l}) | yes p with l ⊑? A
ε-sim ⟨✴⟩₄χ | yes p₁ | yes p = ⟨✴⟩₄χ
ε-sim ⟨✴⟩₄χ | yes p | no ¬p = ⊥-elim (¬p p)
ε-sim (⟨✴⟩₄χ {l = l}) | no ¬p with l ⊑? A
ε-sim ⟨✴⟩₄χ | no ¬p | yes p = ⊥-elim (¬p p)
ε-sim ⟨✴⟩₄χ | no ¬p₁ | no ¬p = ⟨✴⟩∙₃
ε-sim (Relabelχ {h = h} l⊑h) with h ⊑? A
ε-sim (Relabelχ {l = l} l⊑h) | yes p with l ⊑? A
ε-sim (Relabelχ l⊑h) | yes p₁ | yes p = Relabelχ l⊑h
ε-sim (Relabelχ l⊑h) | yes h⊑A | no ¬l⊑A = ⊥-elim (¬l⊑A (trans-⊑ l⊑h h⊑A))
ε-sim (Relabelχ {l = l} l⊑h) | no ¬p with l ⊑? A
ε-sim (Relabelχ {l = l} {h} l⊑h) | no ¬p | yes p = Relabel∙χ l⊑h
ε-sim (Relabelχ {l = l} {h} l⊑h) | no ¬p | no ¬p₁ = Relabel∙₂ l⊑h
ε-sim (Relabel∙χ {l = l} l⊑h) with l ⊑? A
ε-sim (Relabel∙χ {l = l} {h = h} l⊑h) | yes p with h ⊑? A
ε-sim (Relabel∙χ {l = l} {h} l⊑h) | yes p | yes p₁ = Relabel∙χ l⊑h
ε-sim (Relabel∙χ {l = l} {h} l⊑h) | yes p | no ¬p = Relabel∙χ l⊑h
ε-sim (Relabel∙χ {l = l} {h = h} l⊑h) | no ¬p with h ⊑? A
ε-sim (Relabel∙χ {l = l} {h} l⊑h) | no l⋤A | yes h⊑A = ⊥-elim (l⋤A (trans-⊑ l⊑h h⊑A))
ε-sim (Relabel∙χ {l = l} {h} l⊑h) | no ¬p | no ¬p₁ = Relabel∙₂ l⊑h
ε-sim Hole = Hole

-- A high-step does not change the low parts of the store
stepᴴ-≡ : ∀ {H τ} {p p' : Program (Mac H τ)} -> H ⋤ A -> p ⟼ p' -> εˢ (store p) ≡ εˢ (store p')

-- Multiple high-steps do not change the low part of the store
step⋆ᴴ-≡ : ∀ {H τ} {p p' : Program (Mac H τ)} -> H ⋤ A -> p ⟼⋆ p' -> εˢ (store p) ≡ εˢ (store p')
step⋆ᴴ-≡ H⋤A [] = refl
step⋆ᴴ-≡ H⋤A (s ∷ ss)
  rewrite stepᴴ-≡ H⋤A s | step⋆ᴴ-≡ H⋤A ss = refl

stepᴴ-≡ H⋤A (Lift x) = refl
stepᴴ-≡ H⋤A (Label' l⊑h) = refl
stepᴴ-≡ H⋤A (Unlabel₁ l⊑h (Lift x)) = refl
stepᴴ-≡ H⋤A (Unlabel₂ l⊑h) = refl
stepᴴ-≡ H⋤A (Unlabelχ l⊑h) = refl
stepᴴ-≡ H⋤A (Bind₁ step)
  rewrite stepᴴ-≡ H⋤A step = refl
stepᴴ-≡ H⋤A Bind₂ = refl
stepᴴ-≡ H⋤A Bind₃ = refl
stepᴴ-≡ H⋤A (Join l⊑h ⌞ ss , v ⌟)
  rewrite step⋆ᴴ-≡ (trans-⋤ l⊑h H⋤A) ss = refl
stepᴴ-≡ H⋤A (Joinχ l⊑h ⌞ ss , v ⌟)
  rewrite step⋆ᴴ-≡ (trans-⋤ l⊑h H⋤A) ss = refl
stepᴴ-≡ H⋤A (Join∙ l⊑h) = refl
stepᴴ-≡ H⋤A (Catch₁ step)
    rewrite stepᴴ-≡ H⋤A step = refl
stepᴴ-≡ H⋤A Catch₂ = refl
stepᴴ-≡ H⋤A Catch₃ = refl
stepᴴ-≡ H⋤A (New {Σ = Σ} {t = t} l⊑h)
  rewrite newᴴ-≡ Σ ⟦ t ⟧ (trans-⋤ l⊑h H⋤A) = refl
stepᴴ-≡ H⋤A (Write₁ l⊑h (Lift x)) = refl
stepᴴ-≡ H⋤A (Write₂ {Σ = Σ} l⊑h x)
  rewrite writeᴴ-≡ Σ (trans-⋤ l⊑h H⋤A) x = refl
stepᴴ-≡ H⋤A (Read₁ l⊑h (Lift x)) = refl
stepᴴ-≡ H⋤A (Read₂ l⊑h x) = refl
stepᴴ-≡ H⋤A (New∙ l⊑h) = refl
stepᴴ-≡ H⋤A (Write∙₁ l⊑h (Lift x)) = refl
stepᴴ-≡ H⋤A (Write∙₂ l⊑h) = refl
stepᴴ-≡ H⋤A (Fork _) = refl
stepᴴ-≡ H⋤A (Fork∙ _) = refl
stepᴴ-≡ H⋤A (NewMVar {Σ = Σ} {τ = τ} l⊑h)
  rewrite newᴴ-≡ {τ = τ} Σ ⊗ (trans-⋤ l⊑h H⋤A) = refl
stepᴴ-≡ H⋤A (Take₁ (Lift x)) = refl
stepᴴ-≡ H⋤A (Take₂ {Σ = Σ} r w)
  rewrite writeᴴ-≡ Σ H⋤A w = refl
stepᴴ-≡ H⋤A (Put₁ (Lift x)) = refl
stepᴴ-≡ H⋤A (Put₂ {Σ = Σ} r w)
  rewrite writeᴴ-≡ Σ H⋤A w = refl
stepᴴ-≡ H⋤A (NewMVar∙ l⊑h) = refl

-- Single step simulation for impure semantics
εᴾ-sim : ∀ {l τ} {p p' : Program (Mac l τ)} (x : Dec (l ⊑ A)) -> p ⟼ p' -> εᴾ x p ⟼ εᴾ x p'

-- Multiple step simulation for impure semantics
εᴾ-sim⋆ : ∀ {L τ} {Σ Σ'} {t t' : CTerm (Mac L τ)} (L⊑A : L ⊑ A) -> ⟨ Σ , t ⟩ ⟼⋆ ⟨ Σ' , t' ⟩ -> ⟨ εˢ Σ , ε t ⟩ ⟼⋆  ⟨ εˢ Σ' , ε t' ⟩
εᴾ-sim⋆ L⊑A [] = []
εᴾ-sim⋆ L⊑A (s ∷ ss) = εᴾ-sim (yes L⊑A) s ∷ εᴾ-sim⋆ L⊑A ss

εᴾ-sim (yes p) (Lift x) = Lift (ε-sim x)
εᴾ-sim (yes p) (Label' {h = h} l⊑h) with h ⊑? A
... | yes _ = Label' l⊑h
... | no ¬p = Label' l⊑h
εᴾ-sim (yes p) (Unlabel₁ l⊑h (Lift x)) = Unlabel₁ l⊑h (Lift (ε-sim x))
εᴾ-sim (yes p) (Unlabel₂ {l = l} l⊑h) with l ⊑? A
... | yes _ = Unlabel₂ l⊑h
... | no ¬p = ⊥-elim (¬p (trans-⊑ l⊑h p))
εᴾ-sim (yes p) (Unlabelχ {l = l} l⊑h) with l ⊑? A
... | yes _ = Unlabelχ l⊑h
... | no ¬p = ⊥-elim (¬p (trans-⊑ l⊑h p))
εᴾ-sim (yes p) (Bind₁ step) = Bind₁ (εᴾ-sim (yes p) step)
εᴾ-sim (yes p) Bind₂ = Bind₂
εᴾ-sim (yes p) Bind₃ = Bind₃
εᴾ-sim (yes p) (Join {h = h} l⊑h ⌞ ss , v ⌟) with h ⊑? A
εᴾ-sim (yes p) (Join {h = h} l⊑h ⌞ ss , return t ⌟) | yes H⊑A = Join l⊑h ⌞ εᴾ-sim⋆ H⊑A ss , return _ ⌟
εᴾ-sim (yes p) (Join {h = h} l⊑h ⌞ ss , v ⌟) | no H⋤A
  rewrite step⋆ᴴ-≡ H⋤A ss = Join∙ l⊑h
εᴾ-sim (yes p) (Joinχ {h = h} l⊑h ⌞ ss , v ⌟) with h ⊑? A
εᴾ-sim (yes p) (Joinχ {h = h} l⊑h ⌞ ss , throw t ⌟) | yes H⊑A = Joinχ l⊑h ⌞ (εᴾ-sim⋆ H⊑A ss) , throw _ ⌟
εᴾ-sim (yes p) (Joinχ {h = h} l⊑h ⌞ ss , v ⌟) | no H⋤A
    rewrite step⋆ᴴ-≡ H⋤A ss = Join∙ l⊑h
εᴾ-sim (yes p) (Join∙ {h = h} l⊑h) with h ⊑? A
... | yes h⊑A = Join∙ l⊑h
... | no h⋤A = Join∙ l⊑h
εᴾ-sim (yes p) (Catch₁ step) = Catch₁ (εᴾ-sim (yes p) step)
εᴾ-sim (yes p) Catch₂ = Catch₂
εᴾ-sim (yes p) Catch₃ = Catch₃
εᴾ-sim (yes p) (New {l = l} {h} l⊑h) with l ⊑? A
εᴾ-sim (yes p) (New {l = l} {h} l⊑h) | yes p₁ with h ⊑? A
εᴾ-sim (yes p) (New {Σ = Σ} {l = l} {h} {t = t} l⊑h) | yes p₁ | yes h⊑A
  rewrite newᴸ-≡ Σ ⟦ t ⟧ h⊑A with New {Σ = εˢ Σ} {l = l} {h = h} {t = ε t} l⊑h | εˢ-εᴹ-≡ Σ h⊑A | lengthᴹ-≡ Σ h
... | step' | eq₁ | eq₂ rewrite eq₁ | eq₂ = step'
εᴾ-sim (yes p) (New {Σ = Σ} {l = l} {h} {t = t} l⊑h) | yes p₁ | no h⋤A rewrite newᴴ-≡ Σ ⟦ t ⟧ h⋤A = New∙ l⊑h
εᴾ-sim (yes p) (New {l = l} {h} l⊑h) | no ¬p = ⊥-elim (¬p p)
εᴾ-sim (yes p) (Write₁ {h = h} l⊑h (Lift x)) with h ⊑? A
... | yes _ = Write₁ l⊑h (Lift (ε-sim x))
... | no ¬p = Write∙₁ l⊑h (Lift (ε-sim x))
-- Here we check h ⊑? A twice, because evaluation of that with abstraction occurs twice
-- at two different points, namely ε(write _ (Ref h _)) and ε(Ref h _)
εᴾ-sim (yes p) (Write₂ {h = h} l⊑h x) with h ⊑? A
εᴾ-sim (yes p) (Write₂ {h = h} l⊑h x) | yes h⊑A with h ⊑? A
εᴾ-sim (yes p) (Write₂ {Σ} {h = h} {t = t} l⊑h x) | yes h⊑A | yes _
  rewrite writeᴸ-≡ Σ h⊑A x with (εᴹ-write {c = ⟦ t ⟧} Σ h⊑A x ) | εˢ-εᴹ-≡ Σ h⊑A
... | r | eq rewrite sym eq = Write₂ l⊑h r
εᴾ-sim (yes p) (Write₂ {h = h} l⊑h x) | yes h⊑A | no h⋤A = ⊥-elim (h⋤A h⊑A)
εᴾ-sim (yes p) (Write₂ {h = h} l⊑h x) | no ¬p with h ⊑? A
εᴾ-sim (yes p) (Write₂ {h = h} l⊑h x) | no ¬p | yes p₁ = ⊥-elim (¬p p₁)
εᴾ-sim (yes p) (Write₂ {Σ} {h = h} {t = t} l⊑h x) | no h⋤A | no h⋤A₁
  rewrite writeᴴ-≡ Σ h⋤A x = Write∙₂ l⊑h
εᴾ-sim (yes p) (Read₁ l⊑h (Lift x)) = Read₁ l⊑h (Lift (ε-sim x))
εᴾ-sim (yes p) (Read₂ {l = l} l⊑h x) with l ⊑? A
εᴾ-sim (yes p) (Read₂ {Σ} {l = l} {t = t} l⊑h x) | yes l⊑A with (εᴹ-read {c = ⟦ t ⟧} Σ l⊑A x) | εˢ-εᴹ-≡ Σ l⊑A
... | r | eq rewrite sym eq = Read₂ l⊑h r
εᴾ-sim (yes h⊑A) (Read₂ {l = l} l⊑h x) | no l⋤A = ⊥-elim (l⋤A (trans-⊑ l⊑h h⊑A))
εᴾ-sim (yes p) (New∙ {h = h} l⊑h) with h ⊑? A
... | yes _ = New∙ l⊑h
... | no _ = New∙ l⊑h
εᴾ-sim (yes p) (Write∙₁ l⊑h (Lift x)) = Write∙₁ l⊑h (Lift (ε-sim x))
εᴾ-sim (yes p) (Write∙₂ {h = h} l⊑h) with h ⊑? A
... | yes _ = Write∙₂ l⊑h
... | no ¬p = Write∙₂ l⊑h
εᴾ-sim (yes p) (Fork {h = h} l⊑h) with h ⊑? A
... | yes _ = Fork l⊑h
... | no _  = Fork∙ l⊑h
εᴾ-sim (yes p) (Fork∙ l⊑h) = Fork∙ l⊑h
εᴾ-sim (yes p) (NewMVar {Σ} {l} {h} {τ} l⊑h) with h ⊑? A
εᴾ-sim (yes p) (NewMVar {Σ} {l} {h} {τ} l⊑h) | yes h⊑A
  rewrite newᴸ-≡ {τ = τ} Σ ⊗ h⊑A with NewMVar {Σ = εˢ Σ} {τ = τ} l⊑h | εˢ-εᴹ-≡ Σ h⊑A | lengthᴹ-≡ Σ h
... | step' | eq₁ | eq₂ rewrite eq₁ | eq₂ = step'
εᴾ-sim (yes p) (NewMVar {Σ} {l} {h} {τ} l⊑h) | no h⋤A
  rewrite newᴴ-≡ {τ = τ} Σ ⊗ h⋤A = NewMVar∙ l⊑h
εᴾ-sim (yes p) (Take₁ (Lift x)) = Take₁ (Lift (ε-sim x))
εᴾ-sim (yes p) (Take₂ {l = l} r w) with l ⊑? A
εᴾ-sim (yes p) (Take₂ {Σ} {l = l} {t = t} r w) | yes l⊑A
  rewrite writeᴸ-≡ Σ l⊑A w with εᴹ-read {c = ⟦ t ⟧} Σ l⊑A r | εᴹ-write {c = ⊗} Σ l⊑A w | εˢ-εᴹ-≡ Σ l⊑A
... | r' | w' | eq rewrite sym eq = Take₂ r' w'
εᴾ-sim (yes p) (Take₂ {l = l} r w) | no ¬p = ⊥-elim (¬p p)
εᴾ-sim (yes p) (Put₁ (Lift x)) = Put₁ (Lift (ε-sim x))
εᴾ-sim (yes p) (Put₂ {Σ} {l = l} {t = t} r w) with l ⊑? A
εᴾ-sim (yes p) (Put₂ {Σ} {l = l} {t = t} r w) | yes l⊑A
  rewrite writeᴸ-≡ Σ l⊑A w with εᴹ-read {c = ⊗} Σ l⊑A r | εᴹ-write {c = ⟦ t ⟧} Σ l⊑A w | εˢ-εᴹ-≡ Σ l⊑A
... | r' | w' | eq rewrite sym eq = Put₂ r' w'
εᴾ-sim (yes p) (Put₂ {Σ} {l = l} {t = t} r w) | no ¬p = ⊥-elim (¬p p)
εᴾ-sim (yes p) (NewMVar∙ {h = h} l⊑h) with h ⊑? A
... | yes _ = NewMVar∙ l⊑h
... | no _ = NewMVar∙ l⊑h
εᴾ-sim {p = ⟨ Σ₁ , t₁ ⟩} {⟨ Σ₂ , t₂ ⟩} (no H⋤A) step
  rewrite stepᴴ-≡ H⋤A step = Lift Hole
